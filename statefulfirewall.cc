/*
 * statefulipfilter.{cc,hh} -- Stateful IP-packet filter
 *
 */
#define _BSD_SOURCE
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <click/config.h>
#include <click/confparse.hh>
#include "statefulfirewall.hh"
#include <click/error.hh>
#include <click/args.hh>
#include <click/glue.hh>
#include <click/router.hh>
#include <string>
#include <iostream>
#include <cstring>
#include <fstream>
/* Add header files as required*/
CLICK_DECLS
StatefulFirewall::StatefulFirewall();

StatefulFirewall::~StatefulFirewall();

int DEFAULTACTION;
int rd;
int StatefulFirewall::configure(Vector<String> &conf, ErrorHandler *errh)
{
	String pfile;
	int defact;
	if(Args(conf,this,errh)
	.read_mp("POLICYFILE",pfile)
	.read("DEFAULT",defact)
	.complete()<0)
	return -1;
	
	String filename="/home/student-18731/18731/hw1-18731-s19/handout/";
	filename+=pfile;
	//click_chatter("%s",filename.c_str());
	DEFAULTACTION=defact;
	//cout << filename.c_str() << endl;
	rd=read_policy_config(pfile);
	
}

bool StatefulFirewall::check_if_new_connection(const Packet *p)
{
	const click_tcp *tcph=p->tcp_header();
	uint8_t flag=tcph->th_flags;
	if(flag == TH_SYN)
	return true;
	else
	return false;
}

bool StatefulFirewall::check_if_connection_reset(const Packet *p)
{
	const click_tcp *tcph=p->tcp_header();
	uint8_t flag=tcph->th_flags;
	if(flag == TH_RST)
	return true;
	else
	return false;
}

void StatefulFirewall::add_connection(Connection &c, int action)
{
	StatefulFirewall::Connections.insert({c,action});
}

void StatefulFirewall::delete_connection(Connection &c)
{
	StatefulFirewall::Connections.erase(c);
}
//
Connection StatefulFirewall::get_canonicalized_connection(const Packet *p)
{
	const click_ip *iph=p->ip_header();
	const click_tcp *tcph=p->tcp_header();
	//char* srcip;
	//char* dstip;
	//srcip=inet_ntoa(iph->ip_src);
	//dstip=inet_ntoa(iph->ip_dst);
	
	IPAddress sip=IPAddress(iph->ip_src);
	String s1=sip.unparse();
	//srcip=s1.c_str();
	
	IPAddress dip=IPAddress(iph->ip_dst);
	String d1=dip.unparse();
	//dstip=d1.c_str();
	
	uint16_t sport,dport;
	sport=tcph->th_sport;
	dport=tcph->th_dport;
	unsigned long sseq,dseq;
	sseq=(unsigned long)tcph->th_seq;
	dseq=(unsigned long)tcph->th_ack;
	if(strcmp(s1.c_str(),d1.c_str())<0)
	{
		Connection obj(s1.c_str(),d1.c_str(),sport,dport,sseq,dseq,6,true);
		return obj;
	}
	else if(strcmp(s1.c_str(),d1.c_str())>0)
	{
		Connection obj(d1.c_str(),s1.c_str(),sport,dport,sseq,dseq,6,false);
		return obj;
	}
	else
	{
		Connection obj("","",-1,-1,0,0,0,false);	
		return obj;
	}
		
}

int StatefulFirewall::read_policy_config(String s)
{
	String srcip,dstip;
	int i=0,j=0,n_pol=0;
	int srcport,dstport,proto,action;
	const char* c=s.c_str();
	char* tokens;
	string others[100];
	char* temp;
	string line;
	const char* delim=" \n";
	ifstream policies;
	policies.open(c);
	while(policies)
	{
		getline(policies,line);
		//cout << line << endl;
		i++;
		if(i==1)
		continue;
	
		temp=new char[line.length()+1];
		strcpy(temp,line.c_str());
		
		tokens=strtok(temp,delim);
		while(tokens)
		{
			others[j]=tokens;
			tokens=strtok(NULL,delim);
			j++;
		}
		
		//cout << line << endl;
	}
	for(int i=0;i<j;i+=6)
	{
		srcip=others[i].c_str();
		dstip=others[i+2].c_str();
		//cout << srcip << dstip;
		srcport=atoi(others[i+1].c_str());
		dstport=atoi(others[i+3].c_str());
		proto=atoi(others[i+4].c_str());
		action=atoi(others[i+5].c_str());
		//cout << srcport << dstport << proto << action << endl;
		Policy obj(srcip,dstip,srcport,dstport,proto,action);
		list_of_policies.push_back(obj);
		n_pol++;
	}
	return n_pol;

}

//void dotted_addr(const unit32_t *addr,char *s)
//{
//s=ntohs(addr);
//}

int StatefulFirewall::filter_packet(const Packet *p)
{
	int round=0,flag=0;
	const click_ip *iph=p->ip_header();
	const click_tcp *tcph=p->tcp_header();
	//char* srcip;
	//char* dstip;
	

	IPAddress sip=IPAddress(iph->ip_src);
	String s1=sip.unparse();
	//srcip=s1.c_str();
	IPAddress dip=IPAddress(iph->ip_dst);
	String d1=dip.unparse();
	//dstip=d1.c_str();

	uint16_t sport,dport;
	sport=tcph->th_sport;
	dport=tcph->th_dport;
	int s=(int)sport;
	int d=(int)dport;
	unsigned long sseq,dseq;
	sseq=(unsigned long)tcph->th_seq;
	dseq=(unsigned long)tcph->th_ack;
	int act=DEFAULTACTION;
	Connection conn=get_canonicalized_connection(p);
	//cout << "  " << conn.get_sourceip() << " " << conn.get_destip() << " " << conn.get_dport() << " " << conn.get << " " << endl;
	//click_chatter("%s %s %d %d %d",srcip,dstip,s,d,tcph->th_flags);
	if(tcph->th_flags==TH_SYN)
	{

		for(round=0;round<rd;round++)
		{
			if(strcmp(s1.c_str(),list_of_policies[round].get_sourceip().c_str())==0 && strcmp(d1.c_str(),list_of_policies[round].get_destip().c_str())==0)
			{
				//cout << "  " << s1.c_str() << " " << d1.c_str() << " " << list_of_policies[round].get_sourceip().c_str() << " " << list_of_policies[round].get_destip().c_str() << " " << endl;
				act = list_of_policies[round].getAction();
				//cout << act << endl;
			}
		}

		StatefulFirewall::Connections.insert({conn,act});
		return 0;
	}
	else if(tcph->th_flags==TH_RST)
	{
		//cout << StatefulFirewall::Connections.size() << endl;
		for (std::map<Connection,int,cmp_connection>::iterator it=StatefulFirewall::Connections.begin(); it!=StatefulFirewall::Connections.end(); ++it)
		{
			//if(strcmp(conn.get_sourceip().c_str(),it->first.get_sourceip().c_str())==0 && strcmp(conn.get_destip().c_str(),it->first.get_destip().c_str())==0 )
			if(strcmp(conn.get_sourceip().c_str(),it->first.get_sourceip().c_str())==0 && strcmp(conn.get_destip().c_str(),it->first.get_destip().c_str())==0)
			//if(conn.compare(it->first)>0)
			{
				
				StatefulFirewall::Connections.erase(it->first);
			}
		}
		//cout << StatefulFirewall::Connections.size() << endl;
		return 0;
	}
	else
	{
		//if(tcph->th_flags==16)
		//	return 1;
		if(conn.is_forward())
		{
			for (std::map<Connection,int,cmp_connection>::iterator it=StatefulFirewall::Connections.begin(); it!=StatefulFirewall::Connections.end(); ++it)
			{
				//if(strcmp(conn.get_sourceip().c_str(),it->first.get_sourceip().c_str())==0 && strcmp(conn.get_destip().c_str(),it->first.get_destip().c_str())==0)
				if(strcmp(conn.get_sourceip().c_str(),it->first.get_sourceip().c_str())==0 && strcmp(conn.get_destip().c_str(),it->first.get_destip().c_str())==0 && conn.get_dport()==it->first.get_dport() && conn.get_sport()==it->first.get_sport())
				{
					return it->second;
				}
			}
		}
		else
			return 0;
	}
	

}

void StatefulFirewall::push(int port, Packet *p)
{
if(filter_packet(p)==1)
output(1).push(p);
else
output(0).push(p);
}

CLICK_ENDDECLS
EXPORT_ELEMENT(StatefulFirewall)